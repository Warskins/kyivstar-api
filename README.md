# Kyivstar API Parser
This module lets get detailed information about abonents telephone calls in kyivstar virtual mobile network.
## Installation

Install mysql-connector module.
```bash
pip install mysql-connector
```

## Usage
Open config.ini and set your api key and mysql database data.

```config
[mysql]
host =
database =
user =
password =

[main]
_from = 2020-11-07T16:38:47
token =
pause = 3600

```

Run main.py
```python
python main.py