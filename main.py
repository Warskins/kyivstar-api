import requests
from datetime import datetime
import time
from mysql.connector import MySQLConnection, Error
from requests import RequestException
from configparser import ConfigParser


def read_db_config(filename='config.ini', section='mysql'):
    # create parser and read ini configuration file
    parser = ConfigParser()
    parser.read(filename)

    # get section, default to mysql
    db = {}
    if parser.has_section(section):
        items = parser.items(section)
        for item in items:
            db[item[0]] = item[1]
    else:
        raise Exception('{0} not found in the {1} file'.format(section, filename))
    return db


def write_to_mysql(data: list):
    db_setting = read_db_config()  # filename = 'config'   section = 'msq'  default settings
    try:
        mydb = MySQLConnection(**db_setting)
        if not mydb.is_connected():
            print('connection established.')
        else:
            sql = 'INSERT INTO calls (calling_number, called_number, start_time, end_time, duration, calling_name, ' \
                  'called_name) VALUES (%s, %s, %s, %s, ' \
                  '%s, %s, %s) '
            my_cursor = mydb.cursor()
            my_cursor.executemany(sql, data)
            mydb.commit()
            mydb.close()

    except Error as err:
        print(err)


def get_response(_from, _to, token):
    url = "https://fmc.kyivstar.ua/api/cdr/v1/callstat.csv"
    querystring = {
        "token": token,
        "from": _from,
        "to": _to,
        "names": 'on'
    }
    try:
        response = requests.request("GET", url, params=querystring)
        res = [tuple(map(str, sub.split(','))) for sub in response.text.split('\n')[1:-1]]
        return res
    except RequestException as es:
        print(es)
        return get_response(_from, _to, token)


def get_setting(section='main'):
    parser = ConfigParser()
    parser.read('config.ini')

    # get section, default to mysql
    st = {}
    if parser.has_section(section):
        items = parser.items(section)
        for item in items:
            st[item[0]] = item[1]
    else:
        raise Exception('{0} not found in the {1} file'.format(section, 'config.ini'))
    return st


def save_date_settings(date):
    config_object = ConfigParser()
    config_object.read("config.ini")
    config_object.set('main', '_from', str(date))
    with open('config.ini', 'w') as file:
        config_object.write(file)


main_setting = get_setting()
_from = main_setting['_from']
_to = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")


while True:
    resp = get_response(_from, _to, main_setting['token'])  # get response from api
    write_to_mysql(resp)
    print(f'Added {len(resp)} entries from {_from} to {_to}')
    _from = _to
    save_date_settings(_to)
    _to = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
    print('Sleep for {} seconds'.format(main_setting['pause']))
    time.sleep(float(main_setting['pause']))
